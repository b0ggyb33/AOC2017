from input_parser import parse

input_lines = parse('Puzzles/Day4/harry.txt')

total = 0
for line in input_lines:
    line = line.split(' ')
    line_wrong = False
    while len(line) > 0:
        word = line.pop()
        if word in line:
            line_wrong = True
            break
    if not line_wrong:
        total += 1

print('Part 1: {}'.format(total))

for line in input_lines:
    line = line.split(' ')
