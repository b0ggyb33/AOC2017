from input_parser import parse

def process(part):
    input_lines = parse("Puzzles/Day5/input.txt")
    instructions = [int(line) for line in input_lines]
    i = 0
    steps = 0
    while i < len(instructions):
        next_i = i + instructions[i]
        if (instructions[i] >= 3) and (part == 2):
            instructions[i] -= 1
        else:
            instructions[i] += 1
        i = next_i
        steps += 1
    print(steps)

process(1)
process(2)
