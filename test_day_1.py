from collections import deque


def test_two_pairs_offset_1():
    assert 3 == sum_offset_pairs('1122', 1)

def test_all_the_same_offset_1():
    assert 4 == sum_offset_pairs('1111', 1)

def test_alternating_offset_2():
    assert 6 == sum_offset_pairs('1212', 2)

def test_paired_middle_offset_2():
    assert 0 == sum_offset_pairs('1221', 2)


def sum_offset_pairs(input_string, offset):
    input_string = deque(input_string)
    rotated_input = deque(input_string)
    rotated_input.rotate(offset)
    total = 0
    for value, pair in zip(input_string, rotated_input):
        if value == pair:
            total += int(value)
    return total


if __name__ == '__main__':
    from input_parser import parse

    input_string = parse('Puzzles/Day1/harry.txt')[0]
    print(sum_offset_pairs(input_string, 1))
    print(sum_offset_pairs(input_string, len(input_string) / 2))
