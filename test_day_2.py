from unittest import TestCase


def test_min_max_difference_two_numbers():
    assert 1 == range_of_list([1, 2])

def test_range_of_list_example_input():
    assert 8 == range_of_list([5, 1, 9, 5])

def test_range_of_list_with_strings():
    assert 4 == range_of_list(['7', '5', '3'])

def test_sum_ranges_over_list_of_tab_seperated_strings():
    assert 18 == sum_ranges_over_all_lines(['5\t1\t9\t5','7\t5\t3','2\t4\t6\t8'])

def test_find_pair_of_even_divisors_only_elemets():
    assert 2 == evenly_divisible([2, 4])

def test_find_pair_of_even_divisors_longer_list():
    assert 2 == evenly_divisible([4, 3, 6])


def range_of_list(input_list):
    input_list = [int(i) for i in input_list]
    return max(input_list) - min(input_list)

def sum_ranges_over_all_lines(input_lines):
    return sum([range_of_list(i.split('\t')) for i in input_lines])

def sum_division_over_all_lines(input_lines):
    return sum([evenly_divisible(i.split('\t')) for i in input_lines])

def evenly_divisible(input_list):
    input_list = [int(i) for i in input_list]
    while True:
        value = input_list.pop()
        for divisor in input_list:
            if (value % divisor == 0):
                return value / divisor
            elif (divisor % value == 0):
                return divisor / value

if __name__ == '__main__':
    from input_parser import parse

    input_list = parse('Puzzles/Day2/harry.txt')
    print(sum_ranges_over_all_lines(input_list))
    print(sum_division_over_all_lines(input_list))
