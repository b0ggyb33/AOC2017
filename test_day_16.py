from collections import Counter
def dance(programs, move):
    operation = move[0]
    arguments = move[1:]
    if operation == "s":
        move = int(move[1:])
        return programs[-move:] + programs[:-move]
    elif operation == "x":
        x, y = map(int, arguments.split("/"))
        proglist = list(programs)
        proglist[x] = programs[y]
        proglist[y] = programs[x]
    else: #elif operation == "p":
        x, y = arguments.split("/")
        proglist = list(programs)
        idx_x = proglist.index(x)
        idx_y = proglist.index(y)
        proglist[idx_x] = y
        proglist[idx_y] = x
    return "".join(proglist)


def dance_it_all(whole_dance, programs):
    out = programs
    for move in whole_dance.split(","):
        out = dance(out, move)
    return out

def test_can_spin():
    assert "eabcd" == dance("abcde", "s1")


def test_can_spin_2():
    assert "deabc" == dance("abcde", "s2")


def test_can_exchange():
    assert "abced" == dance("abcde", "x3/4")


def test_can_partner():
    assert "aecdb" == dance("abcde", "pe/b")


def test_example_dance():
    whole_dance = "s1,x3/4,pe/b"
    out = dance_it_all(whole_dance, "abcde")
    assert "baedc" == out

from input_parser import parse
def test_p1():
    whole_dance = parse("Puzzles/Day16/rich.txt")[0]
    out = dance_it_all(whole_dance, "abcdefghijklmnop")
    assert "hjknpbamdgofclei" != out

def test_p2():
    whole_dance = parse("Puzzles/Day16/rich.txt")[0]
    out = "abcdefghijklmnop"
    states=[out]
    skip_val = 0
    for i in range(1000000000):
        out = dance_it_all(whole_dance, out)
        if out in states:
            skip_val = len(states)
            break
        states.append(out)
    assert "agndefjhibklmocp" == states[1000000000 % skip_val]
