from collections import defaultdict, Counter
def parse(line):
    first, second = line.split(" <-> ")
    connected_to = map(int,second.split(","))
    return int(first), connected_to

def pipeIt(lines):
    pipes = defaultdict(list)
    for line in lines:
        first,second = parse(line)
        for s in second:
            if first not in pipes[s]:
                pipes[s].append(first)
            if s not in pipes[first]:
                pipes[first].append(s)

    for key,values in pipes.items():
        for value in values:
            for valueXX in pipes[value]:
                if valueXX not in pipes[key]:
                    pipes[key].append(valueXX)
    return pipes

def test_example():

    lines = ["0 <-> 2",
             "1 <-> 1",
             "2 <-> 0, 3, 4",
             "3 <-> 2, 4",
             "4 <-> 2, 3, 6",
             "5 <-> 6",
             "6 <-> 4, 5"]
    groups = pipeIt(lines)

    c = Counter([tuple(sorted(x)) for x in groups.values()])
    print(len(c.keys()))


from input_parser import parse as Parse

def test_part_one():
    input_data = Parse("input.txt")
    groups = pipeIt(input_data)
    print(len(groups[0]))
    c = Counter([tuple(sorted(x)) for x in groups.values()])
    print(len(c.keys()))
