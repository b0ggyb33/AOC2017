from input_parser import parse
import operator

registers = {}
operators = {
             '<':   operator.lt,
             '>':   operator.gt,
             '<=':  operator.le,
             '>=':  operator.ge,
             '==':  operator.eq,
             '!=':  operator.ne,
             'dec': operator.sub,
             'inc': operator.add
            }
instructions = []
conditions = []
input_lines = parse('Puzzles/day8_harry.txt')

for line in input_lines:
    sections = line.split(' if ')
    condition = sections[-1].strip()
    instruction = sections[0].strip()
    condition_components = condition.split(' ')
    condition_register = condition_components[0]
    condition_operator = condition_components[1]
    condition_comparator = condition_components[2]
    instruction_components = instruction.split(' ')
    instruction_register = instruction_components[0]
    instruction_operator = instruction_components[1]
    instruction_amount = instruction_components[2]
    registers.update({condition_register: 0})
    instructions.append((instruction_register, instruction_operator, instruction_amount))
    conditions.append((condition_register, condition_operator, condition_comparator))

max_value = 0
for i in range(len(instructions)):
    if operators[conditions[i][1]](int(registers[conditions[i][0]]), int(conditions[i][2])):
        registers[instructions[i][0]] = operators[instructions[i][1]](registers[instructions[i][0]], int(instructions[i][2]))
    this_max = max(registers.values())
    if max_value < this_max:
        max_value = this_max

print('Part 1: {}'.format(max(registers.values())))
print('Part 2: {}'.format(max_value))
