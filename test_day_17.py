from collections import deque


def n_rotations(n,steps,display=True):
    d = deque()
    for i in range(n):
        d.rotate(-steps)
        d.append(i)
    if display:
        while d[0] != 0:
            d.rotate(1)
    return list(d)

def test_case_1_rotation():
    d = n_rotations(2,steps=3)
    assert d == [0,1]

def test_case_2_rotation():
    d = n_rotations(3,steps=3)
    assert d == [0, 2, 1]

def test_3_rotations():
    d = n_rotations(4,steps=3)
    assert d == [0, 2, 3, 1]

def test_4_rotations():
    d = n_rotations(5,steps=3)
    assert d == [0, 2, 4, 3, 1]

def test_10_rotations():
    d = n_rotations(10,steps=3)
    assert d == [0, 9, 5,  7,  2,  4,  3,  8,  6,  1]

def test_2017_rotations():
    d = n_rotations(2018, steps=3, display=False)
    assert d[0] == 638

def test_one():
    d = n_rotations(2018,steps = 316, display=False)
    assert 180 == d[0]

def test_two():
    d = n_rotations(50000001, steps=316, display=True)
    assert 13326437 == d[1]

