from input_parser import parse
import  numpy as np


def get_example_maze():
    x = ["     |          ",
         "     |  +--+    ",
         "     A  |  C    ",
         " F---|----E|--+ ",
         "     |  |  |  D ",
         "     +B-+  +--+ "]
    return get_maze(x)


def get_maze(instructions):
    arr = np.zeros((len(instructions)+2, len(instructions[0])+2), dtype=np.uint64)
    for i, instruction in enumerate(instructions):
        instruction = [ord(x) for x in instruction]
        arr[i+1, 1:-1] = instruction
    return arr


def find_start(maze):
    return 1, maze[1].argmax()


def move(maze, location, direction, turning=False):
    output = {}
    trial = [location[0]+direction[0], location[1]+ direction[1]]
    current_direction = abs(np.asarray(direction)).argmax()
    test_pieces=[ord("|"), ord("-")]
    if maze[trial[0], trial[1]] == test_pieces[current_direction] or maze[trial[0], trial[1]] == ord("+"):
        output['location'] = trial
        output['direction'] = direction
        output['count'] = 1
        return output
    if ord("A") <= maze[trial[0], trial[1]] <= ord("Z"):
        output['location'] = trial
        output['direction'] = direction
        output['count'] = 1
        output['letter'] = chr(maze[trial[0], trial[1]])
        return output
    if maze[trial[0], trial[1]] == test_pieces[1-current_direction]:
        bigger_trial = [location[0]+2*direction[0], location[1]+ 2*direction[1]]
        if maze[bigger_trial[0], bigger_trial[1]] == test_pieces[current_direction]:
            output['location'] = bigger_trial
            output['direction'] = direction
            output['count'] = 2
            return output
        elif ord("A") <= maze[bigger_trial[0], bigger_trial[1]] <= ord("Z"):
            output['location'] = bigger_trial
            output['direction'] = direction
            output['count'] = 2
            output['letter'] = chr(maze[bigger_trial[0], bigger_trial[1]])
            return output
        else:
            return {}
    else:
        if maze[location[0], location[1]] == ord("+") and not turning:
            # have reached junction and can't continue, must turn
            for directions in [[direction[1],direction[0]], [-direction[1], -direction[0]]]:
                output = move(maze, location, directions, turning=True) # swap direction axis
                if 'direction' in output:
                    return {'direction':output['direction']}
        return {}





def test_is_at_end():
    arr = np.ones((3, 3), dtype=np.uint64)
    arr *= 32
    arr[1, 1] = ord('|')
    location = [1, 1]
    current_direction = [1, 0]

    new_location = move(arr, location, current_direction)


    assert new_location == {}

def test_can_move_north():
    arr = np.ones((3, 3), dtype=np.uint64)
    arr *= 32
    arr[1, 1] = ord('|')
    arr[0, 1] = ord('|')
    location = [1, 1]
    current_direction = [-1, 0]

    output = move(arr, location, current_direction)

    assert output['location'] == [0,1]


def test_can_move_east():
    arr = np.ones((3, 3), dtype=np.uint64)
    arr *= 32
    arr[1, 1] = ord('-')
    arr[1, 0] = ord('-')
    location = [1, 1]
    current_direction = [0, -1]
    output = move(arr, location, current_direction)
    assert output['location'] == [1,0]

def test_can_change_direction():
    arr = np.ones((4, 4), dtype=np.uint64)
    arr *= 32
    arr[2, 2] = ord('-')
    arr[2, 1] = ord('+')
    arr[1, 1] = ord("|")
    location = [2, 1]
    current_direction = [0, -1]
    output = move(arr, location, current_direction)
    assert 'location' not in output
    assert output['direction'] == [-1, 0]

def test_print_letters_when_found():
    arr = np.ones((4, 4), dtype=np.uint64)
    arr *= 32
    arr[2, 3] = ord('-')
    arr[2, 2] = ord('A')
    arr[2, 1] = ord("-")
    location = [2, 3]
    current_direction = [0, -1]
    output = move(arr, location, current_direction)
    assert output['location'] == [2, 2]
    assert output['direction'] == [0, -1]
    assert output['letter'] == 'A'
    output = move(arr, output['location'], output['direction'])
    assert output['location'] == [2, 1]
    assert output['direction'] == [0, -1]


def test_can_move_north_when_overlapping():
    arr = np.ones((3, 3), dtype=np.uint64)
    arr *= 32
    arr[0, 1] = ord('|')
    arr[1, 1] = ord('-')
    arr[2, 1] = ord('|')
    location = [2, 1]
    current_direction = [-1, 0]
    output = move(arr, location, current_direction)
    assert output['location'] == [0, 1]
    assert output['direction'] == [-1, 0]


def test_example():
    m = get_example_maze()
    location = find_start(m)
    direction = [1,0]
    o=1 #not none
    total_count = 1
    letters=[]
    while o != {}:
        o = move(m, location, direction)
        location = o['location'] if 'location' in o else location
        direction = o['direction'] if 'direction' in o else direction
        if 'letter' in o:
            letters.append(o['letter'])
        if 'count' in o:
            total_count += o['count']

    assert "".join(letters) == "ABCDEF"
    assert total_count == 38


def test_p1_and_p2():
    input_instructions = parse('Puzzles/Day19/rich.txt')
    m = get_maze(input_instructions)
    location = find_start(m)
    direction = [1, 0]
    o = 1  # not none
    total_count = 1
    letters = []
    while o != {}:
        o = move(m, location, direction)
        location = o['location'] if 'location' in o else location
        direction = o['direction'] if 'direction' in o else direction
        if 'letter' in o:
            letters.append(o['letter'])
        if 'count' in o:
            total_count += o['count']

    assert "DWNBGECOMY" == "".join(letters)
    assert total_count == 17228