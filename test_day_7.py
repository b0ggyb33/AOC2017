from input_parser import parse
import networkx as nx
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from collections import Counter

input_lines = parse('Puzzles/Day7_harry.txt')

sub_strings_to_remove = '()->,'
g = nx.DiGraph()

for line in input_lines:
    line = line.strip().split(' ')
    parent = line[0]
    children = line[1:]
    for i, string in enumerate(children):
        child = "".join(character for character in string if (character not in sub_strings_to_remove))
        if child.isdigit():
            weight = child
            g.add_node(parent, weight=int(weight))
        elif len(child) > 0:
            g.add_node(child)
            g.add_edge(parent, child)

sorted_nodes = list(nx.topological_sort(g))
print('Part 1: {}'.format(sorted_nodes[0]))

for node in reversed(sorted_nodes):
    if 'sum_weight' not in g.nodes[node]:
        g.add_node(node, sum_weight=g.nodes[node]['weight'])
    total = g.nodes[node]['sum_weight']
    child_weights = [g.nodes[child]['sum_weight'] for child in g[node]]
    child_nodes = [child for child in g[node]]
    unique_weights = set(child_weights)
    if len(unique_weights) > 1:
        counted_weights = Counter(child_weights)
        unbalanced_weight = counted_weights.most_common()[:0:-1][0][0]
        correct_weight = counted_weights.most_common(1)[0][0]
        unbalanced = child_nodes[child_weights.index(unbalanced_weight)]
        diff = correct_weight - unbalanced_weight
        initial_weight = g.nodes[unbalanced]['weight']
        new_weight = initial_weight + diff
        #print('Unbalanced node is {}. It needs to change by {}. Its initial weight was {}, its new weight is {}'.format(unbalanced, diff, initial_weight, new_weight))
        break
    total += sum(child_weights)
    g.add_node(node, sum_weight=total)

print('Part 2: {}'.format(new_weight))
