class Generator:
    def __init__(self, starting_value, factor, multiple=1):
        self.factor = factor
        self.denominator = 2147483647
        self.current_value = starting_value
        self.multiple_criterion = multiple

    def generate(self):
        self.current_value = (self.current_value * self.factor) % self.denominator
        if self.current_value % self.multiple_criterion:
            return self.generate()
        else:
            return self.current_value

    def __eq__(self, other):
        return not self.__ne__(other)

    def __ne__(self, other):
        return (self.current_value & 65535) ^ (other.current_value & 65535)


def judge(Aval, Bval, multipleA, multipleB, iterations):
    g = get_Generator_A(Aval, multipleA)
    h = get_Generator_B(Bval, multipleB)
    sum = 0
    for i in range(iterations):
        g.generate()
        h.generate()
        sum += (g == h)
    return sum


def get_Generator_A(start, multiple=1):
    return Generator(start, 16807, multiple)


def get_Generator_B(start, multiple=1):
    return Generator(start, 48271, multiple)


def test_generator_examples_A():
    g = get_Generator_A(65)
    assert 1092455 == g.generate()
    assert 1181022009 == g.generate()
    assert 245556042 == g.generate()
    assert 1744312007 == g.generate()
    assert 1352636452 == g.generate()


def test_generator_examples_B():
    g = get_Generator_B(8921)
    assert 430625591 == g.generate()
    assert 1233683848 == g.generate()
    assert 1431495498 == g.generate()
    assert 137874439 == g.generate()
    assert 285222916 == g.generate()


def test_maths():
    assert 1 == (1 & 65535)
    assert 0 == (65536 & 65535)
    assert 255 == (65791 & 65535)


def test_compare_generators_returns_false():
    g = get_Generator_A(1)
    h = get_Generator_B(2)
    assert g != h


def test_compare_generators_returns_true():
    g = get_Generator_A(1)
    h = get_Generator_B(1)
    assert g == h


def test_example_comparions_0():
    g = get_Generator_A(0b00000000000100001010101101100111)
    h = get_Generator_B(0b00011001101010101101001100110111)
    assert g != h


def test_example_comparions_1():
    g = get_Generator_A(0b01000110011001001111011100111001)
    h = get_Generator_B(0b01001001100010001000010110001000)
    assert g != h


def test_example_comparions_2():
    g = get_Generator_A(0b00001110101000101110001101001010)
    h = get_Generator_B(0b01010101010100101110001101001010)
    assert g == h


def test_example_comparions_3():
    g = get_Generator_A(0b01100111111110000001011011000111)
    h = get_Generator_B(0b00001000001101111100110000000111)
    assert g != h


def test_example_comparions_4():
    g = get_Generator_A(0b01010000100111111001100000100100)
    h = get_Generator_B(0b00010001000000000010100000000100)
    assert g != h


def test_generator_mod_4():
    g = get_Generator_A(65, 4)
    assert 1352636452 == g.generate()
    assert 1992081072 == g.generate()
    assert 530830436 == g.generate()
    assert 1980017072 == g.generate()
    assert 740335192 == g.generate()


def test_generator_mod_8():
    g = get_Generator_B(8921, 8)
    assert 1233683848 == g.generate()
    assert 862516352 == g.generate()
    assert 1159784568 == g.generate()
    assert 1616057672 == g.generate()
    assert 412269392 == g.generate()


def test_part_one_example():
    assert 588 == judge(65, 8921, 1, 1, 40000000)


def test_part_one():
    assert 573 == judge(634, 301, 1, 1, 40000000)


def test_example_part_two():
    assert 309 == judge(65, 8921, 4, 8, 5000000)


def test_part_two():
    assert 294 == judge(634, 301, 4, 8, 5000000)
