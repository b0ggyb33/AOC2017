from Day10 import process2 as KnotHash
import numpy as np
from scipy.ndimage.measurements import label


def convertToBinary(input_hex):
    binary_number = bin(int(input_hex, 16))[2:]
    return binary_number


def get_grid(input_string):
    grid = np.zeros((128, 128))
    for i in range(128):
        string_input = input_string + "-" + str(i)
        grid_string = convertToBinary(KnotHash(string_input))
        grid[i, -len(grid_string):] = list(map(int, grid_string))
    return grid


def get_num_features(grid):
    _, num_features = label(grid)
    return num_features


def get_num_occupied(grid):
    return grid.sum()


def test_canLoadKnotHash():
    assert KnotHash("flqrgnkx-0") == "d4f76bdcbf838f8416ccfa8bc6d1f9e6"


def test_canConvertToBinary():
    assert convertToBinary("a0c2017") == "1010000011000010000000010111"


def test_string_generated_correctly():
    assert "flqrgnkx-" + str(0) == "flqrgnkx-0"


def test_canConvertActualInput():
    expected = np.array(([1, 1, 0, 1, 0, 1, 0, 0],
                         [0, 1, 0, 1, 0, 1, 0, 1],
                         [0, 0, 0, 0, 1, 0, 1, 0],
                         [1, 0, 1, 0, 1, 1, 0, 1],
                         [0, 1, 1, 0, 1, 0, 0, 0],
                         [1, 1, 0, 0, 1, 0, 0, 1],
                         [0, 1, 0, 0, 0, 1, 0, 0],
                         [1, 1, 0, 1, 0, 1, 1, 0]))
    grid = get_grid("flqrgnkx")
    for i in range(8):
        assert (grid[i, :8] == expected[i]).all()


def test_example_one():
    assert 8108 == get_num_occupied(get_grid("flqrgnkx"))


def test_partone():
    assert 8250 == get_num_occupied(get_grid("stpzcrnm"))


def test_partTwo_example():
    assert 1242 == get_num_features(get_grid("flqrgnkx"))


def test_partTwo_actual():
    assert 1113 == get_num_features(get_grid("stpzcrnm"))
