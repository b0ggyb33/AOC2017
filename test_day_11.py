def test_3_steps_same_direction():
    instructions = ['ne', 'ne', 'ne']
    assert 3 == distance(instructions)

def test_instructions_undo_each_other():
    instructions = ['ne', 'ne', 'sw', 'sw']
    assert 0 == distance(instructions)

def test_north_south_cancel_each_other():
    instructions = ['ne', 'ne', 's', 's']
    assert 2 == distance(instructions)

def test_3_steps():
    assert 3 == distance(['se', 'sw', 'se', 'sw', 'sw'])

def test_one_step():
    assert 1 == distance(['se', 'sw'])

def test_se_and_sw_cancel_out():
    assert distance(['ne', 'ne', 's', 's', 'n']) == distance(['ne', 'nw', 'ne', 's', 's', 'ne'])

def test_one_that_will_fail():
    assert 2 == distance(['s', 'nw', 'nw'])

def test_we_arent_still_wrong():
    input_string = open('Puzzles/Day11/input.txt').readline().rstrip()
    result = distance([string for string in input_string.split(',')])
    assert 896 > result
    assert 477 < result

import numpy as np

def distance(instructions):
    location = np.array([0, 0])
    max_steps = 0
    for instruction in instructions:
        if instruction == 'ne':
            location += [-1, 1]
        elif instruction == 'nw':
            location += [-1, -1]
        elif instruction == 'sw':
            location += [1, -1]
        elif instruction == 'se':
            location += [1, 1]
        elif instruction == 's':
            location += [2, 0]
        elif instruction == 'n':
            location += [-2, 0]
        else:
            assert False
        if steps(location) > max_steps:
            max_steps = steps(location)
    print('Part 2: {}'.format(max_steps))
    return steps(location)

def steps(location):
    x = abs(location[1])
    y = abs(location[0])
    if y > x:
        return x + ((y-x) // 2)
    else:
        return x


if __name__ == '__main__':
    from input_parser import parse
    #input_string = parse('Puzzles/Day11/input.txt')[0]
    input_string = parse('Puzzles/Day11/input_rich.txt')[0].rstrip()
    instructions = [string for string in input_string.split(',')]
    print('Part 1: {}'.format(distance(instructions)))
