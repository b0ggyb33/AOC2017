from collections import defaultdict
import pytest


@pytest.fixture
def registers():
    return Register(0)


class Register:
    def __init__(self, p):
        self.registers = defaultdict(int)
        self.registers['p'] = p
        self.idx = 0
        self.operations = {'snd': self.sound,
                           'set': self.set_value,
                           'add': self.add,
                           'mul': self.mul,
                           'mod': self.mod,
                           'jgz': self.jgz,
                           'rcv': self.receive}

    def isARegister(self, Y):
        try:
            int(Y)
            return False
        except ValueError:
            return True

    def set_instructions(self, instructions):
        self.instructions = instructions

    def parse_next_instruction(self):
        if self.idx < len(self.instructions):
            parts = self.instructions[self.idx].split(' ')
            if (parts[0] == 'rcv') and (self.registers[parts[1]] != 0):
                return self.registers['last']
            else:
                jump = self.operations[parts[0]](*parts[1:])
                if isinstance(jump, int):
                    self.idx += jump
                else:
                    self.idx += 1

    def sound(self, X):
        self.registers['last'] = self.registers[X]
        return self.registers

    def set_value(self, X, Y):
        if self.isARegister(Y):
            self.registers[X] = self.registers[Y]
        else:
            self.registers[X] = int(Y)
        return self.registers

    def add(self, X, Y):
        if self.isARegister(Y):
            self.registers[X] += self.registers[Y]
        else:
            self.registers[X] += int(Y)

        return self.registers

    def mul(self, X, Y):
        if self.isARegister(Y):
            self.registers[X] *= self.registers[Y]
        else:
            self.registers[X] *= int(Y)
        return self.registers

    def mod(self, X, Y):
        if self.isARegister(Y):
            self.registers[X] %= self.registers[Y]
        else:
            self.registers[X] %= int(Y)
        return self.registers

    def jgz(self, X, Y):
        if self.isARegister(X):
            if self.registers[X] <= 0:
                return 1
        else:
            if int(X) <= 0:
                return 1

        if self.isARegister(Y):
            return self.registers[Y]
        else:
            return int(Y)

    def receive(self, X):
        pass

from collections import deque
class Register2(Register):

    def connect(self, other):
        self.other = other
        self.queue = deque()
        self.send_count = 0

    def sound(self, X):
        if self.isARegister(X):
            val = int(self.registers[X])
        else:
            val = int(X)
        self.other.queue.append(val)
        self.send_count+=1

    def receive(self, X):
        try:
            Y = self.queue.popleft()
        except IndexError:
            return 0
        else:
            self.set_value(X,Y)
            return None

    def parse_next_instruction(self):
        if self.idx < len(self.instructions):
            parts = self.instructions[self.idx].split(' ')
            jump = self.operations[parts[0]](*parts[1:])
            if isinstance(jump, int):
                if jump == 0:
                    return "locked"
                else:
                    self.idx += jump
            else:
                self.idx += 1
        else:
            return "locked"


def test_play_sound(registers):
    registers.sound('a')
    assert registers.registers['last'] == 0


def test_set_X_to_Y(registers):
    registers.set_value('a', 1)
    assert registers.registers['a'] == 1


def test_add_Y_to_X(registers):
    registers.add('a', 2)
    assert registers.registers['a'] == 2


def test_mul_X_by_Y(registers):
    registers.registers['a'] = 2
    registers.mul('a', 'a')
    assert registers.registers['a'] == 4


def test_mul_X_by_int(registers):
    registers.mul('a', -1)
    assert registers.registers['a'] == 0


def test_jgz_Y_if_X_greater_than_zero(registers):
    registers.registers['a'] = 1
    assert registers.jgz('a', -1) == -1


def test_jgz_1_if_X_equals_zeros(registers):
    registers.registers['a'] = 0
    assert registers.jgz('a', -1) == 1


def test_parse_instruction(registers):
    registers.parse_instructions(['snd a'])
    assert registers.registers['last'] == 0

def test_register_val_Y_true():
    assert Register.isARegister(None,"a")

def test_register_val_1_false():
    assert not Register.isARegister(None,"1")

def test_register_val_minus1_false():
    assert not Register.isARegister(None,"-1")



def test_example_part_one(registers):
    instructions = ['set a 1',
                    'add a 2',
                    'mul a a',
                    'mod a 5',
                    'snd a',
                    'set a 0',
                    'rcv a',
                    'jgz a -1',
                    'set a 1',
                    'jgz a -2']

    assert registers.parse_instructions(instructions) == 4


def test_part_one():
    from input_parser import parse
    register = Register(0)
    instructions = parse('Puzzles/Day18/input.txt')
    register.set_instructions(instructions)
    output = None
    while output is None:
        output = register.parse_next_instruction()
    assert output == 1187


def test_example_two():
    register0 = Register2(0)
    register1 = Register2(1)
    instructions = ["snd 1",
                    "snd 2",
                    "snd p",
                    "rcv a",
                    "rcv b",
                    "rcv c",
                    "rcv d"]
    register0.set_instructions(instructions)
    register1.set_instructions(instructions)
    register0.connect(register1)
    register1.connect(register0)
    output0 = None
    output1 = None
    while output0 is None and output1 is None:
        output0 = register0.parse_next_instruction()
        output1 = register1.parse_next_instruction()
    assert register1.registers['c'] == 0
    assert register0.registers['c'] == 1
    assert register1.send_count == 3

def test_example_two_out_of_sync():
    register0 = Register2(0)
    register1 = Register2(1)
    instructions = ["snd 1",
                    "snd 2",
                    "snd p",
                    "rcv a",
                    "rcv b",
                    "rcv c",
                    "rcv d"]
    register0.set_instructions(instructions)
    register1.set_instructions(instructions)
    register0.connect(register1)
    register1.connect(register0)
    output0 = None
    output1 = None
    idx = 0
    while output0 is None:
        output0 = register0.parse_next_instruction()
        if output0 is not None:
            break
        idx += 1
    assert idx == 3
    idx=0
    while output1 is None:
        output1 = register1.parse_next_instruction()
        if output1 is not None:
            break
        idx += 1
    assert idx == 6
    idx=0
    output0 = None
    while output0 is None:
        output0 = register0.parse_next_instruction()
        if output0 is not None:
            break
        idx += 1
    assert idx == 3

    assert output0 == 'locked' and output1 == 'locked'

    assert register1.send_count == 3

def test_cantreceive():
    register0 = Register2(0)
    register1 = Register2(1)
    instructions = ['jgz p 2', 'rcv a', 'snd 0', 'rcv a']
    register0.set_instructions(instructions)
    register1.set_instructions(instructions)
    register0.connect(register1)
    register1.connect(register0)
    register0.parse_next_instruction()  # jgx
    assert register0.parse_next_instruction() == "locked"  # rcv
    register1.parse_next_instruction()  # jgx
    register1.parse_next_instruction()  # snd 1
    assert register1.send_count == 1
    assert register1.parse_next_instruction() == "locked"  # 2nd rcv a
    assert register0.idx == 1
    register0.parse_next_instruction()  # rcv
    assert register0.idx == 2
    register0.parse_next_instruction()  # snd
    assert register0.send_count == 1
    assert not register1.parse_next_instruction() == "locked"  # rcv
    assert register1.idx == 4
    assert register1.parse_next_instruction() == "locked"
    assert register0.parse_next_instruction() == "locked"  # rcv fails
    assert register0.idx == 3

def test_part_two():
    from input_parser import parse

    register0 = Register2(0)
    register1 = Register2(1)
    instructions = parse('Puzzles/Day18/rich.txt')
    register0.set_instructions(instructions)
    register1.set_instructions(instructions)
    register0.connect(register1)
    register1.connect(register0)
    output0 = None
    output1 = None

    while output0 is None or output1 is None:
        output0 = register0.parse_next_instruction()
        output1 = register1.parse_next_instruction()
    assert 7112 == register1.send_count