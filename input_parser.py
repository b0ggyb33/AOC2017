def parse(filename):
    """
    Generates a list of strings for each line in the puzzle input
    """
    return [line.rstrip("\n") for line in open(filename).readlines()]
