import pytest

@pytest.fixture
def knot():
    return Knot([0, 1, 2, 3, 4])

def test_initialise_list():
    knot = Knot([0, 1, 2, 3, 4])
    assert knot.list == [0, 1, 2, 3, 4]

def test_select_sublist_length_three(knot):
    assert [0, 1, 2] == knot.select_sublist(3, 0)

def test_current_index_starts_at_0(knot):
    assert knot.index == 0

def test_select_sublist_starting_at_index_2(knot):
    assert knot.select_sublist(3, 2) == [2, 3, 4]

def test_reverse_sublist(knot):
    assert knot.reverse_sublist([0, 1, 2]) == [2, 1, 0]

def test_insert_sublist(knot):
    assert knot.insert_sublist([2, 1, 0], 0, 3) == [2, 1, 0, 3, 4]

def test_insert_sublist_in_centre(knot):
    assert knot.insert_sublist([3, 2, 1], 1, 3) == [0, 3, 2, 1, 4]

def test_skip_size_increases_on_inserting_into_list(knot):
    assert knot.skip == 0
    knot.insert_sublist([1], 1, 1)
    assert knot.skip == 1

def test_index_increases_by_length_plus_skip(knot):
    assert knot.index == 0
    knot.skip = 1
    knot.insert_sublist([1], 1, 1)
    assert knot.index == 2

def test_select_sublist_wrapping_over_end(knot):
    assert knot.select_sublist(3, 3) == [3, 4, 0]

def test_insert_sublist_wrapping_over_end(knot):
    assert knot.insert_sublist([0, 4, 3], 3, 3) == [3, 1, 2, 0, 4]

def test_index_wraps_around_when_increasing_over_length_of_list(knot):
    knot.index = 4
    knot.insert_sublist([1], 4, 1)
    assert knot.index == 0

def test_process(knot):
    lengths = [3, 4, 1, 5]
    assert knot.process(lengths) == [3, 4, 2, 1, 0]
    assert knot.index == 4
    assert knot.skip == 4

def test_process_manually_defined():
    knot = Knot()
    lengths = [256]
    assert knot.process(lengths) == list(range(256))[::-1]

def test_part_2_empty():
    knot = Knot(list(range(256)))
    assert knot.process_part_2([]) == 'a2582a3a0e66e6e86e3812dcb672a272'

def test_part_2_AoC():
    knot = Knot(list(range(256)))
    assert knot.process_part_2([ord(x) for x in 'AoC 2017'.strip()]) == '33efeb34ea91902bb2f59c9920caa6cd'

def test_part_2_123():
    knot = Knot(list(range(256)))
    assert knot.process_part_2([ord(x) for x in '1,2,3'.strip()]) == '3efbe78a8d82f29979031a4aa0b16a9d'

def test_part_2_124():
    knot = Knot(list(range(256)))
    assert knot.process_part_2([ord(x) for x in '1,2,4'.strip()]) == '63960835bcdc130f0b66d7ff4f6a5a8e'


from operator import xor

class Knot:
    def __init__(self, initial_list=list(range(256))):
        self.list = initial_list
        self.index = 0
        self.skip = 0

    def select_sublist(self, length, start):
        if start + length > len(self.list):
            extra_length = length - (len(self.list) - start)
            sublist = self.list[start:] + self.list[:extra_length]
        else:
            sublist = self.list[start:start+length]
        return sublist

    def reverse_sublist(self, sublist):
        return sublist[::-1]

    def insert_sublist(self, sublist, position, length):
        if length + position > len(self.list):
            extra_length = length - (len(self.list) - position)
            self.list[position:] = sublist[:-extra_length]
            self.list[:extra_length] = sublist[-extra_length:]
        else:
            self.list[position:position + length] = sublist
        self.index = (self.index + length + self.skip) % len(self.list)
        self.skip += 1
        return self.list

    def process(self, lengths):
        for length in lengths:
            self.insert_sublist(self.reverse_sublist(self.select_sublist(length, self.index)), self.index, length)
        return self.list

    def process_part_2(self, lengths):
        lengths.extend([17, 31, 73, 47, 23])
        for _ in range(64):
            self.list = self.process(lengths)
        result = ''
        for i in range(0, 16):
            sublist = self.list[i*16:(i+1)*16]
            result += '%02x' % reduce(xor, sublist)
        return result

if __name__ == '__main__':
    from input_parser import parse

    knot = Knot(list(range(256)))
    initial_input = parse('Puzzles/Day10/input.txt')[0].strip()
    input_lengths = [int(x) for x in initial_input.strip().split(',')]
    new_list = knot.process(input_lengths)
    print('Part 1: {}'.format(new_list[0] * new_list[1]))
    knot = Knot(list(range(256)))
    input_lengths = [ord(x) for x in initial_input.strip()]
    result = knot.process_part_2(input_lengths)
    print('Part 2: {}'.format(result))
