import numpy as np


def test_redistribute_moves_one_on_one_bank():
    assert [0, 1] == redistribute([1, 0])


def test_redistribute_moves_one_back():
    assert [1, 0] == redistribute([0, 1])


def test_redistribute_2_1_becomes_1_2():
    assert [1, 2] == redistribute([2, 1])


def test_redistribute_2_1_2_becomes_0_2_3():
    assert [0, 2, 3] == redistribute([2, 1, 2])


def test_2_1_2_has_been_seen_at_idx_1():
    previous = [[2, 1, 1], [2, 1, 2]]
    current = [2, 1, 2]
    assert 1 == has_been_seen(previous, current)


def test_2_1_2_has_not_been_seen_at_idx_1():
    previous = [[2, 1, 1], [2, 1, 3]]
    current = [2, 1, 2]
    assert -1 == has_been_seen(previous, current)


def test_2_1_has_been_seen_at_idx_2():
    previous = [[2, 1, 1], [2, 1, 3], [2, 1, 2]]
    current = [2, 1, 2]
    assert 2 == has_been_seen(previous, current)


def redistribute(input_data):
    idx = np.argmax(input_data)
    memory = input_data[idx]
    input_data[idx] = 0

    for _ in range(memory):
        idx += 1
        idx %= len(input_data)
        input_data[idx] += 1

    return input_data


def has_been_seen(previous, current):
    if current in previous:
        count = 0
        previous.reverse()
        while True:
            item = previous.pop()
            if current == item:
                return count
            else:
                count += 1
    else:
        return -1


def detect_and_count_loop(inp):
    seen = []
    count = 0
    while True:
        count += 1
        seen.append(inp[:])
        inp = redistribute(inp)
        val = has_been_seen(seen, inp)
        if val > -1:
            return count, count - val


def test_example():
    assert 5, 4 == detect_and_count_loop([0, 2, 7, 0])


def test_both():
    assert 4074, 2793 == detect_and_count_loop([11, 11, 13, 7, 0, 15, 5, 5, 4, 4, 1, 1, 7, 1, 15, 11])

