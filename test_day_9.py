def find_garbage(string_with_garbage):
    start = -1
    idx = 0
    collected = 0
    while idx < len(string_with_garbage):
        if string_with_garbage[idx] == "!":
            idx += 2
            continue

        elif start > -1:
            if string_with_garbage[idx] == ">":
                end = idx
                return (start, end), collected
            else:
                collected += 1

        elif string_with_garbage[idx] == "<":
            if start == -1:
                start = idx

        idx += 1


def remove_all_garbage(string_with_garbage, count_garbage=False):
    out = "notNone"
    total_garbage_collected = 0
    while out is not None:
        out = find_garbage(string_with_garbage)
        if out is not None:
            out, garbage_collected = out
            string_with_garbage = string_with_garbage[:out[0]] + string_with_garbage[out[1] + 1:]
            total_garbage_collected += garbage_collected
    if count_garbage:
        return string_with_garbage, total_garbage_collected
    else:
        return string_with_garbage


def find_group(string_with_groups, noscore=True):
    string_with_groups = remove_all_garbage(string_with_groups)
    groups = 0
    idx = 0
    toResolve = 0
    score = 0
    nesting = 0
    while idx < len(string_with_groups):
        if string_with_groups[idx] == "{":
            toResolve += 1
            nesting += 1
        if string_with_groups[idx] == "}":
            toResolve -= 1
            groups += 1
            score += nesting
            nesting -= 1
        idx += 1
    if noscore:
        return groups
    else:
        return score


def find_garbage_test(string, start=0):
    assert (start, len(string) - 1) == find_garbage(string)[0]


def find_group_test(string, groups):
    assert groups == find_group(string)


def group_score_test(string, score):
    assert score == find_group(string, noscore=False)


def count_garbage_test(string, collected):
    out, amount = find_garbage(string)
    assert amount == collected


def test_find_no_garbage():
    assert None == find_garbage("this has no garbage")


def test_find_empty_garbage():
    find_garbage_test("<>")


def test_random_garbage():
    find_garbage_test("<random characters>")


def test_ignored_opens_in_garbage():
    find_garbage_test("<<<<>")


def test_ignored_close_because_exclamation_in_garbage():
    find_garbage_test("<{!>}>")


def test_find_close_because_second_exclamation_is_ignored_in_garbage():
    find_garbage_test("<!!>")


def test_second_exc_and_first_close_are_ignored_in_garbage():
    find_garbage_test("<!!!>>")


def test_example_closes_at_end():
    find_garbage_test(r'<{o"i!a,<{i<a>')


def test_finds_garbage_when_not_at_start():
    find_garbage_test(r'00<{o"i!a,<{i<a>', 2)


def test_empty_grouping():
    find_group_test("{}", 1)


def test_triple_nesting():
    find_group_test("{{{}}}", 3)


def test_6_nesting():
    find_group_test("{{{},{},{{}}}}", 6)


def test_remove_all_garbage():
    assert remove_all_garbage("{<{},{},{{}}>}") == "{}"


def test_one_group_with_garbage():
    find_group_test("{<{},{},{{}}>}", 1)


def test_fiond_one_group_with_multiple_garbage():
    find_group_test("{<a>,<a>,<a>,<a>}", 1)


def test_find_five_with_multiple_garbage():
    find_group_test("{{<a>},{<a>},{<a>},{<a>}}", 5)


def test_find_two_with_garbage_and_cancelling():
    find_group_test("{{<!>},{<!>},{<!>},{<a>}}", 2)


def test_empty_grouping_scores_one():
    group_score_test("{}", 1)


def test_triple_nesting_scores_6():
    group_score_test("{{{}}}", 6)


def test_5_nesting():
    group_score_test("{{},{}}", 5)


def test_quad_nesting_scores_16():
    group_score_test("{{{},{},{{}}}}", 16)


def test_one_group_with_garbage_scores_1():
    group_score_test("{<a>,<a>,<a>,<a>}", 1)


def test_fiond_five_group_with_multiple_garbage_and_nesting_is_9():
    group_score_test("{{<ab>},{<ab>},{<ab>},{<ab>}}", 9)


def test_exclamation_thing_is_9():
    group_score_test("{{<!!>},{<!!>},{<!!>},{<!!>}}", 9)


def test_final_one():
    group_score_test("{{<a!>},{<a!>},{<a!>},{<ab>}}", 3)


from input_parser import parse


def test_partOne():
    str = parse("day9.txt")[0]
    assert 15922 == find_group(str, noscore=False)


def test_no_garbage_collected():
    count_garbage_test("<>", 0)


def test_17_collected():
    count_garbage_test("<random characters>", 17)


def test_3_collected():
    count_garbage_test("<<<<>", 3)


def test_some_escaping():
    count_garbage_test("<{!>}>", 2)


def test_nothing_with_two_esc():
    count_garbage_test("<!!>", 0)


def test_most_confusing_count():
    count_garbage_test(r'<{o"i!a,<{i<a>', 10)


def test_two():
    str = parse("day9.txt")[0]
    assert 7314 == remove_all_garbage(str, count_garbage=True)[1]
