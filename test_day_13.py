from input_parser import parse


def get_example_wall():
    input_lines = ["0: 3",
                   "1: 2",
                   "4: 4",
                   "6: 4"]
    return build_a_wall(input_lines)


def build_a_wall(input_lines):
    wall = []
    for line in input_lines:
        depth, range = map(int, line.split(":"))
        wall.append((depth, range))
    return wall


def layer_collision(layer, time):
    depth, range = layer
    if 0 == (depth + time) % ((2 * range) - 2):
        return True


def layer_severity(layer, time):
    depth, range = layer
    if 0 == (depth + time) % ((2 * range) - 2):
        return depth * range
    else:
        return 0


def part_one(wall, t):
    return sum([layer_severity((depth, r), t) for depth, r in wall])


def no_collisions(wall, t):
    return not any([layer_collision((depth, r), t) for depth, r in wall])


def part_two(wall):
    delay_time = 0
    while True:
        if no_collisions(wall, delay_time):
            return delay_time
        else:
            delay_time += 1


def test_can_calculate_severity_in_one_layer():
    wall = get_example_wall()
    assert 0 == layer_severity(wall[0], 0)


def test_can_calculate_severity_in_different_layer():
    wall = get_example_wall()
    assert 2 == layer_severity(wall[1], 1)


def test_severity_is_0_when_position_is_not_zero():
    wall = get_example_wall()
    assert 0 == layer_severity(wall[1], 2)


def test_severity_of_example_is_24():
    wall = get_example_wall()
    trip_severity = part_one(wall, 0)
    assert 24 == trip_severity


def test_severity_part_one():
    lines = parse("day13.txt")
    wall = build_a_wall(lines)
    assert 2160 == part_one(wall, 0)


def test_example_2():
    wall = get_example_wall()
    assert 10 == part_two(wall)


def test_part_two():
    lines = parse("day13.txt")
    wall = build_a_wall(lines)
    time = part_two(wall)
    assert time == 3907470
