input_line = [0, 5, 10, 0, 11, 14, 13, 4, 11, 8, 8, 7, 1, 4, 12, 11]
seen_states = []

while True:
    value_to_distribute = max(input_line)
    i = input_line.index(max(input_line))
    input_line[i] = 0
    while value_to_distribute > 0:
        if i + 1 == len(input_line):
            i = 0
        else:
            i += 1
        input_line[i] += 1
        value_to_distribute -= 1
    if input_line in seen_states:
        seen_states.append(list(input_line))
        break
    seen_states.append(list(input_line))
print(len(seen_states))
print((len(seen_states)) - (seen_states.index(input_line) + 1))
